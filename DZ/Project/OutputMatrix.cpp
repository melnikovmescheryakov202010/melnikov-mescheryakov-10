#include <iostream>
#include <ctime>

using namespace std;

void OutputMatrix(int** currentMatrix, const int size)
{
    cout << "\n" << "Matrix:" << endl;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            cout << currentMatrix[i][j] << '\t';
        }
        cout << '\n';
    }
}