#include <iostream>
#include <ctime>

using namespace std;

void ShowEqualRows(int** currentMatrix, const int size)
{
    int  i, topLine, lowerLine, numberOne, numberTwo;
    bool equalRow;
    lowerLine = size;
    cout << "\n";
    for (topLine = 0; topLine < (size / 2); topLine++)
    {
        equalRow = true;
        for (i = 0; i < size; i++)
        {
            numberOne = currentMatrix[topLine][i];
            if (numberOne != (numberTwo = 1))
            {
                equalRow = false;
                break;
            }
        }
        lowerLine--;
        cout << "(" << topLine << " and " << size - topLine << ")" << " - ";
        ((equalRow == false) ? (cout << " not equals" << endl) : ((cout << "  equals" << endl)));
    }
}