#pragma once
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

void GenerateMatrix(int** currentMatrix, const int size);
void OutputMatrix(int** currentMatrix, const int size);
void ShowEqualRows(int** currentMatrix, const int size);
void ShowSumAfterNegative(int** currentMatrix, const int size);
void ShowBlackZoneAmount(int** currentMatrix, const int size);

#endif