#include <iostream>
#include <ctime>

using namespace std;

void ShowBlackZoneAmount(int** currentMatrix, const int size)
{
    int sum = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (i + j <= size - 1)
            {
                sum += currentMatrix[i][j];
            }
        }
    }
    cout << "\n" << "Black zone amount: " << sum << endl;
}