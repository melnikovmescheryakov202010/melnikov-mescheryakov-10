#include <iostream>
#include <ctime>
#include "Functions.h"

using namespace std;

int main()
{
    int matrixSize = 0;

    cout << "Enter matrix size:" << endl;
    cin >> matrixSize;

    int** matrix = new int* [matrixSize];

    for (int i = 0; i < matrixSize; i++)
    {
        matrix[i] = new int[matrixSize];
    }

    GenerateMatrix(matrix, matrixSize);
    OutputMatrix(matrix, matrixSize);
    ShowEqualRows(matrix, matrixSize);
    ShowSumAfterNegative(matrix, matrixSize);
    ShowBlackZoneAmount(matrix, matrixSize);

    for (int i = 0; i < matrixSize; i++)
    {
        delete[]matrix[i];
    }

    delete[]matrix;
    cin.get();
}