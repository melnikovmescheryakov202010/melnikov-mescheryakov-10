#include <iostream>
#include <ctime>

using namespace std;

void ShowSumAfterNegative(int** currentMatrix, const int size)
{
    int sum, counter, i, rowNumber;
    cout << "\n";
    for (rowNumber = 0; rowNumber < size; rowNumber++)
    {
        counter = -1;
        sum = 0;
        for (i = 0; i < size; i++)
        {
            if (currentMatrix[rowNumber][i] < 0)
            {
                counter = i;
                break;
            }
        }
        if (counter >= 0)
        {
            for (i = ++counter; i < size; i++)
            {
                sum += abs(currentMatrix[rowNumber][i]);
            }
        }
        cout << "sum = (" << rowNumber + 1 << ")" << sum << endl;
    }
}