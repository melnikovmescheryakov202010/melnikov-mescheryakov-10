#include <iostream>
#include <ctime>

using namespace std;

void GenerateMatrix(int** currentMatrix, const int size)
{
    srand(time(NULL));

    int border;
    cout << "Enter border:" << endl;
    cin >> border;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            currentMatrix[i][j] = ((rand() % border + 1) * ((rand() % 2 == 0) ? 1 : -1));
        }
    }
}